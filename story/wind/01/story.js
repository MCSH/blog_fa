// Created with Squiffy 4.0.0
// https://github.com/textadventures/squiffy

(function(){
/* jshint quotmark: single */
/* jshint evil: true */

var squiffy = {};

(function () {
    'use strict';

    squiffy.story = {};
    squiffy.story.begin = function () {
        squiffy.ui.output.on('click', 'a.squiffy-link', function (event) {
            if ($(this).hasClass('disabled')) return;
            var passage = $(this).data('passage');
            var section = $(this).data('section');
            var rotateAttr = $(this).attr('data-rotate');
            var sequenceAttr = $(this).attr('data-sequence');
            if (passage) {
                $(this).addClass('disabled');
                squiffy.set('_turncount', squiffy.get('_turncount') + 1);
                passage = processLink(passage);
                if (passage) {
                    currentSection.append('<hr/>');
                    squiffy.story.passage(passage);
                }
                var turnPassage = '@' + squiffy.get('_turncount');
                if (turnPassage in squiffy.story.section.passages) {
                    squiffy.story.passage(turnPassage);
                }
            }
            else if (section) {
                currentSection.append('<hr/>');
                $(this).addClass('disabled');
                section = processLink(section);
                squiffy.story.go(section);
            }
            else if (rotateAttr || sequenceAttr) {
                var result = rotate(rotateAttr || sequenceAttr, rotateAttr ? $(this).text() : '');
                $(this).html(result[0].replace(/&quot;/g, '"').replace(/&#39;/g, '\''));
                var dataAttribute = rotateAttr ? 'data-rotate' : 'data-sequence';
                $(this).attr(dataAttribute, result[1]);
                if (!result[1]) {
                    $(this).addClass('disabled');
                }
                if ($(this).attr('data-attribute')) {
                    squiffy.set($(this).attr('data-attribute'), result[0]);
                }
                squiffy.story.save();
            }
        });
        squiffy.ui.output.on('mousedown', 'a.squiffy-link', function (event) {
            event.preventDefault();
        });
        if (!squiffy.story.load()) {
            squiffy.story.go(squiffy.story.start);
        }
    };

    var processLink = function(link) {
        var sections = link.split(',');
        var first = true;
        var target = null;
        sections.forEach(function (section) {
            section = section.trim();
            if (startsWith(section, '@replace ')) {
                replaceLabel(section.substring(9));
            }
            else {
                if (first) {
                    target = section;
                }
                else {
                    setAttribute(section);
                }
            }
            first = false;
        });
        return target;
    };

    var setAttribute = function(expr) {
        var lhs, rhs, op, value;
        var setRegex = /^([\w]*)\s*=\s*(.*)$/;
        var setMatch = setRegex.exec(expr);
        if (setMatch) {
            lhs = setMatch[1];
            rhs = setMatch[2];
            if (isNaN(rhs)) {
                squiffy.set(lhs, rhs);
            }
            else {
                squiffy.set(lhs, parseFloat(rhs));
            }
        }
        else {
            var incDecRegex = /^([\w]*)\s*([\+\-])=\s*(.*)$/;
            var incDecMatch = incDecRegex.exec(expr);
            if (incDecMatch) {
                lhs = incDecMatch[1];
                op = incDecMatch[2];
                rhs = parseFloat(incDecMatch[3]);
                value = squiffy.get(lhs);
                if (value === null) value = 0;
                if (op == '+') {
                    value += rhs;
                }
                if (op == '-') {
                    value -= rhs;
                }
                squiffy.set(lhs, value);
            }
            else {
                value = true;
                if (startsWith(expr, 'not ')) {
                    expr = expr.substring(4);
                    value = false;
                }
                squiffy.set(expr, value);
            }
        }
    };

    var replaceLabel = function(expr) {
        var regex = /^([\w]*)\s*=\s*(.*)$/;
        var match = regex.exec(expr);
        if (!match) return;
        var label = match[1];
        var text = match[2];
        if (text in squiffy.story.section.passages) {
            text = squiffy.story.section.passages[text].text;
        }
        else if (text in squiffy.story.sections) {
            text = squiffy.story.sections[text].text;
        }
        var stripParags = /^<p>(.*)<\/p>$/;
        var stripParagsMatch = stripParags.exec(text);
        if (stripParagsMatch) {
            text = stripParagsMatch[1];
        }
        var $labels = squiffy.ui.output.find('.squiffy-label-' + label);
        $labels.fadeOut(1000, function() {
            $labels.html(squiffy.ui.processText(text));
            $labels.fadeIn(1000, function() {
                squiffy.story.save();
            });
        });
    };

    squiffy.story.go = function(section) {
        squiffy.set('_transition', null);
        newSection();
        squiffy.story.section = squiffy.story.sections[section];
        if (!squiffy.story.section) return;
        squiffy.set('_section', section);
        setSeen(section);
        var master = squiffy.story.sections[''];
        if (master) {
            squiffy.story.run(master);
            squiffy.ui.write(master.text);
        }
        squiffy.story.run(squiffy.story.section);
        // The JS might have changed which section we're in
        if (squiffy.get('_section') == section) {
            squiffy.set('_turncount', 0);
            squiffy.ui.write(squiffy.story.section.text);
            squiffy.story.save();
        }
    };

    squiffy.story.run = function(section) {
        if (section.clear) {
            squiffy.ui.clearScreen();
        }
        if (section.attributes) {
            processAttributes(section.attributes);
        }
        if (section.js) {
            section.js();
        }
    };

    squiffy.story.passage = function(passageName) {
        var passage = squiffy.story.section.passages[passageName];
        if (!passage) return;
        setSeen(passageName);
        var masterSection = squiffy.story.sections[''];
        if (masterSection) {
            var masterPassage = masterSection.passages[''];
            if (masterPassage) {
                squiffy.story.run(masterPassage);
                squiffy.ui.write(masterPassage.text);
            }
        }
        var master = squiffy.story.section.passages[''];
        if (master) {
            squiffy.story.run(master);
            squiffy.ui.write(master.text);
        }
        squiffy.story.run(passage);
        squiffy.ui.write(passage.text);
        squiffy.story.save();
    };

    var processAttributes = function(attributes) {
        attributes.forEach(function (attribute) {
            if (startsWith(attribute, '@replace ')) {
                replaceLabel(attribute.substring(9));
            }
            else {
                setAttribute(attribute);
            }
        });
    };

    squiffy.story.restart = function() {
        if (squiffy.ui.settings.persist && window.localStorage) {
            var keys = Object.keys(localStorage);
            $.each(keys, function (idx, key) {
                if (startsWith(key, squiffy.story.id)) {
                    localStorage.removeItem(key);
                }
            });
        }
        else {
            squiffy.storageFallback = {};
        }
        if (squiffy.ui.settings.scroll === 'element') {
            squiffy.ui.output.html('');
            squiffy.story.begin();
        }
        else {
            location.reload();
        }
    };

    squiffy.story.save = function() {
        squiffy.set('_output', squiffy.ui.output.html());
    };

    squiffy.story.load = function() {
        var output = squiffy.get('_output');
        if (!output) return false;
        squiffy.ui.output.html(output);
        currentSection = $('#' + squiffy.get('_output-section'));
        squiffy.story.section = squiffy.story.sections[squiffy.get('_section')];
        var transition = squiffy.get('_transition');
        if (transition) {
            eval('(' + transition + ')()');
        }
        return true;
    };

    var setSeen = function(sectionName) {
        var seenSections = squiffy.get('_seen_sections');
        if (!seenSections) seenSections = [];
        if (seenSections.indexOf(sectionName) == -1) {
            seenSections.push(sectionName);
            squiffy.set('_seen_sections', seenSections);
        }
    };

    squiffy.story.seen = function(sectionName) {
        var seenSections = squiffy.get('_seen_sections');
        if (!seenSections) return false;
        return (seenSections.indexOf(sectionName) > -1);
    };
    
    squiffy.ui = {};

    var currentSection = null;
    var screenIsClear = true;
    var scrollPosition = 0;

    var newSection = function() {
        if (currentSection) {
            $('.squiffy-link', currentSection).addClass('disabled');
        }
        var sectionCount = squiffy.get('_section-count') + 1;
        squiffy.set('_section-count', sectionCount);
        var id = 'squiffy-section-' + sectionCount;
        currentSection = $('<div/>', {
            id: id,
        }).appendTo(squiffy.ui.output);
        squiffy.set('_output-section', id);
    };

    squiffy.ui.write = function(text) {
        screenIsClear = false;
        scrollPosition = squiffy.ui.output.height();
        currentSection.append($('<div/>').html(squiffy.ui.processText(text)));
        squiffy.ui.scrollToEnd();
    };

    squiffy.ui.clearScreen = function() {
        squiffy.ui.output.html('');
        screenIsClear = true;
        newSection();
    };

    squiffy.ui.scrollToEnd = function() {
        var scrollTo, currentScrollTop, distance, duration;
        if (squiffy.ui.settings.scroll === 'element') {
            scrollTo = squiffy.ui.output[0].scrollHeight - squiffy.ui.output.height();
            currentScrollTop = squiffy.ui.output.scrollTop();
            if (scrollTo > currentScrollTop) {
                distance = scrollTo - currentScrollTop;
                duration = distance / 0.4;
                squiffy.ui.output.stop().animate({ scrollTop: scrollTo }, duration);
            }
        }
        else {
            scrollTo = scrollPosition;
            currentScrollTop = Math.max($('body').scrollTop(), $('html').scrollTop());
            if (scrollTo > currentScrollTop) {
                var maxScrollTop = $(document).height() - $(window).height();
                if (scrollTo > maxScrollTop) scrollTo = maxScrollTop;
                distance = scrollTo - currentScrollTop;
                duration = distance / 0.5;
                $('body,html').stop().animate({ scrollTop: scrollTo }, duration);
            }
        }
    };

    squiffy.ui.processText = function(text) {
        function process(text, data) {
            var containsUnprocessedSection = false;
            var open = text.indexOf('{');
            var close;
            
            if (open > -1) {
                var nestCount = 1;
                var searchStart = open + 1;
                var finished = false;
             
                while (!finished) {
                    var nextOpen = text.indexOf('{', searchStart);
                    var nextClose = text.indexOf('}', searchStart);
         
                    if (nextClose > -1) {
                        if (nextOpen > -1 && nextOpen < nextClose) {
                            nestCount++;
                            searchStart = nextOpen + 1;
                        }
                        else {
                            nestCount--;
                            searchStart = nextClose + 1;
                            if (nestCount === 0) {
                                close = nextClose;
                                containsUnprocessedSection = true;
                                finished = true;
                            }
                        }
                    }
                    else {
                        finished = true;
                    }
                }
            }
            
            if (containsUnprocessedSection) {
                var section = text.substring(open + 1, close);
                var value = processTextCommand(section, data);
                text = text.substring(0, open) + value + process(text.substring(close + 1), data);
            }
            
            return (text);
        }

        function processTextCommand(text, data) {
            if (startsWith(text, 'if ')) {
                return processTextCommand_If(text, data);
            }
            else if (startsWith(text, 'else:')) {
                return processTextCommand_Else(text, data);
            }
            else if (startsWith(text, 'label:')) {
                return processTextCommand_Label(text, data);
            }
            else if (/^rotate[: ]/.test(text)) {
                return processTextCommand_Rotate('rotate', text, data);
            }
            else if (/^sequence[: ]/.test(text)) {
                return processTextCommand_Rotate('sequence', text, data);   
            }
            else if (text in squiffy.story.section.passages) {
                return process(squiffy.story.section.passages[text].text, data);
            }
            else if (text in squiffy.story.sections) {
                return process(squiffy.story.sections[text].text, data);
            }
            return squiffy.get(text);
        }

        function processTextCommand_If(section, data) {
            var command = section.substring(3);
            var colon = command.indexOf(':');
            if (colon == -1) {
                return ('{if ' + command + '}');
            }

            var text = command.substring(colon + 1);
            var condition = command.substring(0, colon);

            var operatorRegex = /([\w ]*)(=|&lt;=|&gt;=|&lt;&gt;|&lt;|&gt;)(.*)/;
            var match = operatorRegex.exec(condition);

            var result = false;

            if (match) {
                var lhs = squiffy.get(match[1]);
                var op = match[2];
                var rhs = match[3];

                if (op == '=' && lhs == rhs) result = true;
                if (op == '&lt;&gt;' && lhs != rhs) result = true;
                if (op == '&gt;' && lhs > rhs) result = true;
                if (op == '&lt;' && lhs < rhs) result = true;
                if (op == '&gt;=' && lhs >= rhs) result = true;
                if (op == '&lt;=' && lhs <= rhs) result = true;
            }
            else {
                var checkValue = true;
                if (startsWith(condition, 'not ')) {
                    condition = condition.substring(4);
                    checkValue = false;
                }

                if (startsWith(condition, 'seen ')) {
                    result = (squiffy.story.seen(condition.substring(5)) == checkValue);
                }
                else {
                    var value = squiffy.get(condition);
                    if (value === null) value = false;
                    result = (value == checkValue);
                }
            }

            var textResult = result ? process(text, data) : '';

            data.lastIf = result;
            return textResult;
        }

        function processTextCommand_Else(section, data) {
            if (!('lastIf' in data) || data.lastIf) return '';
            var text = section.substring(5);
            return process(text, data);
        }

        function processTextCommand_Label(section, data) {
            var command = section.substring(6);
            var eq = command.indexOf('=');
            if (eq == -1) {
                return ('{label:' + command + '}');
            }

            var text = command.substring(eq + 1);
            var label = command.substring(0, eq);

            return '<span class="squiffy-label-' + label + '">' + process(text, data) + '</span>';
        }

        function processTextCommand_Rotate(type, section, data) {
            var options;
            var attribute = '';
            if (section.substring(type.length, type.length + 1) == ' ') {
                var colon = section.indexOf(':');
                if (colon == -1) {
                    return '{' + section + '}';
                }
                options = section.substring(colon + 1);
                attribute = section.substring(type.length + 1, colon);
            }
            else {
                options = section.substring(type.length + 1);
            }
            var rotation = rotate(options.replace(/"/g, '&quot;').replace(/'/g, '&#39;'));
            if (attribute) {
                squiffy.set(attribute, rotation[0]);
            }
            return '<a class="squiffy-link" data-' + type + '="' + rotation[1] + '" data-attribute="' + attribute + '" role="link">' + rotation[0] + '</a>';
        }

        var data = {
            fulltext: text
        };
        return process(text, data);
    };

    squiffy.ui.transition = function(f) {
        squiffy.set('_transition', f.toString());
        f();
    };

    squiffy.storageFallback = {};

    squiffy.set = function(attribute, value) {
        if (typeof value === 'undefined') value = true;
        if (squiffy.ui.settings.persist && window.localStorage) {
            localStorage[squiffy.story.id + '-' + attribute] = JSON.stringify(value);
        }
        else {
            squiffy.storageFallback[attribute] = JSON.stringify(value);
        }
        squiffy.ui.settings.onSet(attribute, value);
    };

    squiffy.get = function(attribute) {
        var result;
        if (squiffy.ui.settings.persist && window.localStorage) {
            result = localStorage[squiffy.story.id + '-' + attribute];
        }
        else {
            result = squiffy.storageFallback[attribute];
        }
        if (!result) return null;
        return JSON.parse(result);
    };

    var startsWith = function(string, prefix) {
        return string.substring(0, prefix.length) === prefix;
    };

    var rotate = function(options, current) {
        var colon = options.indexOf(':');
        if (colon == -1) {
            return [options, current];
        }
        var next = options.substring(0, colon);
        var remaining = options.substring(colon + 1);
        if (current) remaining += ':' + current;
        return [next, remaining];
    };

    var methods = {
        init: function (options) {
            var settings = $.extend({
                scroll: 'body',
                persist: true,
                restartPrompt: true,
                onSet: function (attribute, value) {}
            }, options);

            squiffy.ui.output = this;
            squiffy.ui.restart = $(settings.restart);
            squiffy.ui.settings = settings;

            if (settings.scroll === 'element') {
                squiffy.ui.output.css('overflow-y', 'auto');
            }

            squiffy.story.begin();
            
            return this;
        },
        get: function (attribute) {
            return squiffy.get(attribute);
        },
        set: function (attribute, value) {
            squiffy.set(attribute, value);
        },
        restart: function () {
            if (!squiffy.ui.settings.restartPrompt || confirm('Are you sure you want to restart?')) {
                squiffy.story.restart();
            }
        }
    };

    $.fn.squiffy = function (methodOrOptions) {
        if (methods[methodOrOptions]) {
            return methods[methodOrOptions]
                .apply(this, Array.prototype.slice.call(arguments, 1));
        }
        else if (typeof methodOrOptions === 'object' || ! methodOrOptions) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method ' +  methodOrOptions + ' does not exist');
        }
    };
})();

var get = squiffy.get;
var set = squiffy.set;

squiffy.story.start = '_default';
squiffy.story.id = '33112cd4ba';
squiffy.story.sections = {
	'_default': {
		'text': "<p><a class=\"squiffy-link link-passage\" data-passage=\"هدفون\" role=\"link\">هدفون</a> به گوش تو <a class=\"squiffy-link link-passage\" data-passage=\"ایستگاه\" role=\"link\">ایستگاه</a> منتظر قطار ایستاده بودم. چند دقیقه‌ای میشد که اینجا رسیده بودم و <a class=\"squiffy-link link-passage\" data-passage=\"می‌دونستم\" role=\"link\">می‌دونستم</a> قطار ۴ ۵ دقیقه دیگه درها رو باز می‌کنه.</p>",
		'passages': {
			'می‌دونستم': {
				'text': "<p>از مهرماه تقریبا هر روز با همین قطار همین <a class=\"squiffy-link link-passage\" data-passage=\"ساعت\" role=\"link\">ساعت</a> حرکت کردم. </p>",
			},
			'ساعت': {
				'text': "<p>ساعت حرکت اولین قطار به سمت <a class=\"squiffy-link link-passage\" data-passage=\"تهران\" role=\"link\">تهران</a> ۵:۳۰ صبح هست و من هر روز خودم رو به موقع رسوندم.</p>",
			},
			'ایستگاه': {
				'text': "<p>ایستگاه متروی گلشهر، انتهای خط ۵ مترو. با خونمون فاصله کمی داشت و ارزون‌ترین راه رفتن به <a class=\"squiffy-link link-passage\" data-passage=\"تهران\" role=\"link\">تهران</a> بود. زیاد شلوغ بود اما من خیلی ازش بدم نمی‌اومد.</p>",
			},
			'تهران': {
				'text': "<p>تهران رو خیلی دوست نداشتم، اما خب انتخاب دیگه‌ای هم نداشتم. تمام امکانات - کار و تحصیل و فروشگاه‌ها و قیمت مناسب - توی تهران جمع شده بودن و من مجبور بودم برای برخوردار شدن ازشون به تهران بیام.</p>",
			},
			'هدفون': {
				'text': "<p>معمولا این ساعت آهنگ کوش می‌کردم تا کامل بیدار شم، اما از اول این هفته برای فرار از روزمرگی به <a class=\"squiffy-link link-passage\" data-passage=\"پادکست\" role=\"link\">پادکست</a> رو آوردم.</p>",
			},
			'پادکست': {
				'text': "<p>الآن نزدیک ده دقیقه‌است دارم به بحث داغی درباره نیاز یا عدم نیاز به یک سری قاعده اخلاقی برای ربات‌ها گوش می‌دم، دید آدم‌ها خیلی می‌تونه متفاوت باشه.</p>",
			},
			'@3': {
				'text': "",
				'js': function() {
					squiffy.story.go('باز شدن در');
				},
			},
		},
	},
	'باز شدن در': {
		'text': "<p>در قطار باز شد. سوار میشم. همراه با <a class=\"squiffy-link link-passage\" data-passage=\"من\" role=\"link\">من</a> دو نفر دیگه هم وارد میشن، مردی که به وضوح <a class=\"squiffy-link link-passage\" data-passage=\"کارگر\" role=\"link\">کارگر</a>ه و یک <a class=\"squiffy-link link-passage\" data-passage=\"پسر جوان\" role=\"link\">پسر جوان</a>.</p>\n<p>سر <a class=\"squiffy-link link-passage\" data-passage=\"جای همیشگیم\" role=\"link\">جای همیشگیم</a> نشستم.</p>",
		'passages': {
			'من': {
				'text': "<p>دانشجوی سال اول هستم و کله‌ام هنوز داغه و سری دارم به سنگ نخورده با هزار امید و آرزو.</p>",
			},
			'کارگر': {
				'text': "<p>کارگر لباسی کهنه اما مرتب به تن داره و پیری دستاش، حتا با وجود این فاصله، قابل تشخیصه. بهش نمی‌خوره بیشتر از ۳۰ سال سن داشته باشه اما چهره خسته‌ای داره.</p>",
			},
			'پسر جوان': {
				'text': "<p>پسر چهره خوابآلودی داره که به زور باز نگهشون داشته. کوله سنگین پشتش و حضورش این ساعت تو مترو نشون میده اون هم دانشجوئه. قبلتر هم این ساعت دیدمش.</p>",
			},
			'جای همیشگیم': {
				'text': "<p><a class=\"squiffy-link link-passage\" data-passage=\"پایین\" role=\"link\">پایین</a>، در جهت خلاف قطار و به سمتی که <a class=\"squiffy-link link-passage\" data-passage=\"آفتاب\" role=\"link\">آفتاب</a> تو چشمم نزنه.</p>",
			},
			'آفتاب': {
				'text': "<p>البته این موقع سال تا نیمه‌های مسیر آفتاب در نمیاد، اما خب می‌دونم وقتی در بیاد اذیت میشم.</p>",
			},
			'پایین': {
				'text': "<p> همیشه دوست داشتم بالا برم اما خیلی زود یاد گرفتم که اکثر مردم همین تمایل رو دارن و صندلی‌های طبقه بالا زودتر پر میشه، من هم اولویت اولم نشستنه و برای همین <a class=\"squiffy-link link-passage\" data-passage=\"تصمیم\" role=\"link\">تصمیم</a> گرفتم پایین بشینم.</p>",
			},
			'تصمیم': {
				'text': "<p>از وقتی این تصمیم رو گرفتم تقریبا هر روز جای نشستن گیرم اومد.</p>",
			},
			'@5': {
				'text': "",
				'js': function() {
					squiffy.story.go('حرکت قطار');
				},
			},
		},
	},
	'حرکت قطار': {
		'text': "<p>قطار به حرکت می‌افته. <a class=\"squiffy-link link-passage\" data-passage=\"دختر و پسری جوان\" role=\"link\">دختر و پسری جوان</a> در ردیف مقابل نشستن و <a class=\"squiffy-link link-passage\" data-passage=\"مردی حدودا چهل ساله\" role=\"link\">مردی حدودا چهل ساله</a> روبروشون نشسته.</p>",
		'passages': {
			'@5': {
				'text': "",
				'js': function() {
					squiffy.story.go('پیاده شدن');
				},
			},
			'دختر و پسری جوان': {
				'text': "<p><a class=\"squiffy-link link-passage\" data-passage=\"دختر\" role=\"link\">دختر</a> و <a class=\"squiffy-link link-passage\" data-passage=\"پسر\" role=\"link\">پسر</a> شاید مجموعا چهل سال نداشته باشن. بهم چسبیدن و در گوش هم نجوا می‌کنن.</p>",
			},
			'دختر': {
				'text': "<p>دخترک قیافه مظلومی داره اما اون رو پشت <a class=\"squiffy-link link-passage\" data-passage=\"پیام‌های طعنه داری\" role=\"link\">پیام‌های طعنه داری</a> که به کیفش آویزون کرده قایم کرده.</p>",
			},
			'پیام‌های طعنه داری': {
				'text': "<p>روی یکی از استیکرها نوشته If you are reading this get your head out of my ass و روی دیگری نوشته Sorry about your face</p>",
			},
			'پسر': {
				'text': "<p>پسر قیافه معقولی داره، عینکی نیم‌فریم به روی صورتش هست و با آرامش به دخترک گوش میده. <a class=\"squiffy-link link-passage\" data-passage=\"کتاب\" role=\"link\">کتاب</a>ی توی دستشه که محکم گرفته‌اش.</p>",
			},
			'کتاب': {
				'text': "<p>کتاب بیشعوری، برام جالبه که نسخه چاپی این کتاب رو می‌بینم. تا جایی که خاطرم هست مجوز چاپ نگرفته بود. <a class=\"squiffy-link link-section\" data-section=\"درباره کتاب ازش بپرسم؟\" role=\"link\">درباره کتاب ازش بپرسم؟</a></p>",
			},
			'مردی حدودا چهل ساله': {
				'text': "<p>مرد با ریشی بلند رو به روی پسر و دختر نشسته و با تسبیحی که دستشه مشغول به دعاست.</p>",
			},
		},
	},
	'درباره کتاب ازش بپرسم؟': {
		'text': "<p>«ببخشید کتابی که دستتونه مگه مجوز چاپ داره؟ از کجا تهیه کردین؟»</p>\n<p>با صدایی بلند شروع به داد زدن می‌کنه: «به شما چه آقا؟ مگه قراره هر کاری رو انجام می‌دیم از فیلتر شماها رد کنیم؟ زندگیمون رو به اینجا کشیدین کافی نیست؟»</p>\n<p>دخترک دست پسر رو محکم گرفته و ازش می‌خواد آروم باشه «ولم کن نگین، اینا هر کاری می‌خوان می‌کنن، به خصوصی‌ترین چیزای ما هم کار دارن! آخه نشد که!»</p>\n<p><a class=\"squiffy-link link-section\" data-section=\"ساکت وایسم تا قطار به مقصد برسه\" role=\"link\">ساکت وایسم تا قطار به مقصد برسه</a> یا <a class=\"squiffy-link link-section\" data-section=\"جوابشو بدم؟\" role=\"link\">جوابشو بدم؟</a></p>",
		'passages': {
		},
	},
	'جوابشو بدم؟': {
		'text': "<p>«مگه چی گفتم دوستِ من؟ صرفا برام جالب بود، من pdf این کتاب رو خوندم و یادمه نویسنده از مجوز نگرفتنش شاکی بود»</p>\n<p>پسر، همچنان با نگاهی عصبانی اما این بار آروم‌تر جواب داد «مگه شماها هم از این کتابا می‌خونید؟ خداتون ناراحت نمیشه اونوقت؟»</p>\n<p>نمی‌دونم از چی برداشت کرده که من آدم خشکه مذهبی باشم. اوقاتم تلخ شده، آروم بهش می‌گم «همه طوری که به نظرت میرسن فکر نمی‌کنن، خدای من اونقدر بیکار نیست که به کتابم کار داشته باشه.»</p>",
		'js': function() {
			setTimeout(()=>{squiffy.story.go('پیاده شدن');}, 200);
		},
		'passages': {
		},
	},
	'ساکت وایسم تا قطار به مقصد برسه': {
		'text': "<p>دستم رو مشت می‌کنم و آماده میشم تا عصبانیتم رو سرش خالی کنم. من رو با <a class=\"squiffy-link link-passage\" data-passage=\"کی\" role=\"link\">کی</a> اشتباه گرفته مگه؟ هدفونم رو تو گوشم می‌کنم و از پنجره <a class=\"squiffy-link link-passage\" data-passage=\"بیرون\" role=\"link\">بیرون</a> رو نگاه می‌کنم. <a class=\"squiffy-link link-passage\" data-passage=\"صدای بلند و تند حرف زدن\" role=\"link\">صدای بلند و تند حرف زدن</a> پسرک از توی هدفون هم به گوشم می‌رسه. </p>",
		'passages': {
			'صدای بلند و تند حرف زدن': {
				'text': "<p>انگار دارن <a class=\"squiffy-link link-passage\" data-passage=\"دعوا\" role=\"link\">دعوا</a> می‌کنن. </p>",
			},
			'دعوا': {
				'text': "<p>دخترک ازش ناراحت شده که چرا با وجود این که ازش خواسته، به داد زدن سر من ادامه داده.</p>",
			},
			'کی': {
				'text': "<p>من رو بسیجی فرض کرده؟ نیستم. البته خب بهش حق میدم، <a class=\"squiffy-link link-passage\" data-passage=\"لباسم\" role=\"link\">لباسم</a> ساده است و صورتم کمی ریش داره... اما باز دلیل نمیشه از روی ظاهر قضاوت کنه. مهم نیست، من آرومم.</p>",
			},
			'لباسم': {
				'text': "<p>شلوار جین و پیراهن مردونه و یک کت ساده به تن دارم، کیفی هم همراهمه و عینک به چشم دارم.</p>",
			},
			'بیرون': {
				'text': "<p>خورشید تازه درومده، هوا کمی ابر داره. ای‌کاش امروز بارون میومد، خیلی وقته دلم بارون می‌خواد.</p>",
			},
			'@2': {
				'text': "",
				'js': function() {
					squiffy.story.go('پیاده شدن');
				},
			},
		},
	},
	'پیاده شدن': {
		'text': "<p>قطار به ایستگاه صادقیه رسیده. بی توجه به دختر و پسر و دعواهاشون، از قطار پیاده میشم، امروز روز عجیبی در انتظارمه و قراره روزمرگی‌مو کنار بذارم.</p>\n<p>ادامه دارد. من رو در <a href=\"https://t.me/pi_developer\">تلگرام</a> و <a href=\"https://twitter.com/Sajjad_Heydari\">توییتر</a> دنبال کنید</p>",
		'passages': {
		},
	},
	'': {
		'text': "",
		'js': function() {
			let tmp = squiffy.get('_seen_sections');
			tmp = tmp[tmp.length-1];
			gtag('event', 'story', {
			  event_category: 'section',
			  event_label: tmp,
			});
		},
		'passages': {
			'': {
				'text': "",
				'js': function() {
					let tmp = squiffy.get('_seen_sections');
					tmp = tmp[tmp.length-1];
					gtag('event', 'story', {
					  event_category: 'passage',
					  event_label: tmp,
					});
				},
			},
		},
	},
}
})();